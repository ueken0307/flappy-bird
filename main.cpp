#define _USE_MATH_DEFINES 
//↑math.hでM_PIを使うための定義

//-------ヘッダーファイル-------
#include "DxLib.h"
#include<math.h>
#include<stdlib.h>
#include<time.h>
//------------------------------

//---------定数の定義-----------
//-ウィンドウサイズ-
#define _DEPTH_ 300
#define _WIDTH_ 300
//--スクロール速度--
#define _SPEED_ 3
//↑300の約数
//-地面画像のサイズ-
#define _P_SIZE_X_ 300
#define _P_SIZE_Y_ 50
//--キャラのサイズ--
#define _C_SIZE_ 25
//--キャラのX座標---
#define _C_X_ 30
//-ブロックのサイズ-
#define _B_SIZE_X_ 35
//-ブロック同士の幅-
//上下
#define _SPACE_Y_ 75
//左右
#define _SPACE_X_ 200
//------------------------------

//--------グローバル変数--------
//----キー入力用----
int key[256];
//--------色--------
unsigned int blue,white,red;
//----マウス関連----
int mouse_x, mouse_y;
int before_click=0;
//--キャラ座標関連--
int c_y = 125, c_y_a = 125,start_y;
//---地面座標関連---
int g1_x = 0, g1_y = _DEPTH_ - 50, g2_x = _P_SIZE_X_, g2_y = _DEPTH_ - 50;
//-ブロック座標関連-
int b1_x, b1_y, b2_x, b2_y;
//---ジャンプ関連---
int jump_count=0, f_jump = 0,i_fall=0;
//---その他フラグ---
int start_flag = 0,add_score=0;
//-----画像関連-----
int ground_1, ground_2, b_top, b_ben, bird;
//-------得点-------
int score = 0;
//---総クリック数---
long total_click = 0;
//------------------------------

//----関数のプロトタイプ宣言----
int updatekey(void);
int click(void);
void input(void);
void Draw(void);
void setup(void);
//変数の初期化用
//----ゲーム関連----
void game(void);
void jump(void);
void fall(void);
void scroll(void);
int check_hit(void);
//------------------------------

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	//------------------------------------------------
	ChangeWindowMode(TRUE);
	SetDrawScreen(DX_SCREEN_BACK);
	SetMouseDispFlag(TRUE);
	SetGraphMode(_WIDTH_,_DEPTH_, 32);
	SetWindowText("手抜きfLAPPY bIRD");
	//------------------------------------------------

	//-------------------
	blue = GetColor(0, 128, 255);
	white = GetColor(255, 255, 255);
	red = GetColor(255,0,0);
	//-------------------

	srand((unsigned)time(NULL));

	

	//******************
	DxLib_Init();
	//******************

	ground_1 = LoadGraph("pic/ground1.png");
	ground_2 = LoadGraph("pic/ground2.png");
	b_top = LoadGraph("pic/block_1.png");
	b_ben = LoadGraph("pic/block_2.png");
	bird = LoadGraph("pic/bird.png");

	setup();
	
	while (ScreenFlip() == 0 && ProcessMessage() == 0 && ClearDrawScreen() == 0 &&
		updatekey() == 0 &&GetMousePoint(&mouse_x,&mouse_y)==0){

		if (start_flag ==0){
			if (click() != 0){
			start_flag = 1;
			}
		}
		if (start_flag == 1){
			game();
		}
		Draw();
	}
	DxLib_End();
	return 0;
}

int updatekey(void){
	char tmpkey[256];
	int i;

	GetHitKeyStateAll(tmpkey);
	for (i = 0; i<256; i++){
		if (tmpkey[i] != 0){
			key[i]++;
		}
		else{
			key[i] = 0;
		}
	}

	return 0;
}

int click(){
	if ((0 <= mouse_x&&mouse_x <= _WIDTH_) && (0 <= mouse_y&&mouse_x <= _DEPTH_)){
		if (before_click == 0 && (GetMouseInput() & MOUSE_INPUT_LEFT) != 0){
			before_click = 1;
			return 1;
		}
		if ((GetMouseInput() & MOUSE_INPUT_LEFT) != 0){
			before_click = 1;
		}
		else{
			before_click = 0;
		}

	}
	return 0;
}

void input(){

	if (click() == 1){
		total_click++;
		if (f_jump == 0){
			f_jump = 1;
			i_fall = 0;
		}

		start_y = c_y;
		jump_count = 0;
	}

}

void Draw(){
	DrawBox(0, 0, _WIDTH_, _DEPTH_, blue, TRUE);
	DrawExtendGraph(g1_x, g1_y, g1_x + _P_SIZE_X_, g1_y + _P_SIZE_Y_, ground_1, FALSE);
	DrawExtendGraph(g2_x, g2_y, g2_x + _P_SIZE_X_, g2_y + _P_SIZE_Y_, ground_2, FALSE);

	DrawExtendGraph(_C_X_, c_y, _C_X_ + _C_SIZE_, c_y + _C_SIZE_, bird, FALSE);

	//-------------------//
	//ブロック一つ目
	DrawExtendGraph(b1_x, 0, b1_x + _B_SIZE_X_, b1_y - 10, b_ben, FALSE);
	DrawExtendGraph(b1_x - 5, b1_y - 10, b1_x + _B_SIZE_X_ +5, b1_y, b_top, FALSE);
	DrawExtendGraph(b1_x, b1_y + 10 + _SPACE_Y_, b1_x + _B_SIZE_X_, _DEPTH_ - _P_SIZE_Y_, b_ben, FALSE);
	DrawExtendGraph(b1_x - 5, b1_y + _SPACE_Y_, b1_x + _B_SIZE_X_ + 5, b1_y + 10 + _SPACE_Y_, b_top, FALSE);
	//ブロック二つ目
	DrawExtendGraph(b2_x, 0, b2_x + _B_SIZE_X_, b2_y - 10, b_ben, FALSE);
	DrawExtendGraph(b2_x - 5, b2_y - 10, b2_x + _B_SIZE_X_ + 5, b2_y, b_top, FALSE);
	DrawExtendGraph(b2_x, b2_y + 10 + _SPACE_Y_, b2_x + _B_SIZE_X_, _DEPTH_ - _P_SIZE_Y_, b_ben, FALSE);
	DrawExtendGraph(b2_x - 5, b2_y + _SPACE_Y_, b2_x + _B_SIZE_X_ + 5, b2_y + 10 + _SPACE_Y_, b_top, FALSE);
	//--------------------//
	
	if (start_flag == 0){
		DrawFormatString(_WIDTH_ / 2 - GetDrawFormatStringWidth("pLEASE cLICK!") / 2, _DEPTH_ / 2, white, "pLEASE cLICK!");
	}
	if (start_flag == 1){
		DrawFormatString(_WIDTH_ / 2 - GetDrawFormatStringWidth("%d", score) / 2,10, white, "%d", score);
		DrawFormatString(_WIDTH_ - GetDrawFormatStringWidth("tOTAL cLICK : %ld",total_click), _DEPTH_ -15, white, "tOTAL cLICK:%ld", total_click);
		
	}
}

void setup(){
	//--キャラ座標関連--
	c_y = 125;
	c_y_a = 125;
	//---地面座標関連---
	g1_x = 0;
	g1_y = _DEPTH_ - 50;
	g2_x = _P_SIZE_X_;
	g2_y = _DEPTH_ - 50;
	//-ブロック座標関連-
	b1_x = _WIDTH_ + 10;
	b2_x = _WIDTH_ + 10 + _SPACE_X_;
	//---ジャンプ関連---
	jump_count = 0;
	f_jump = 0;
	i_fall = 0;
	//------得点-------
	score = 0;
	add_score = 0;
}

void game(){
	bool gameover = FALSE;
	int gameover_back_c = 255;
	unsigned int gameover_back;

	b1_y = rand() % (220 - _SPACE_Y_) + 15;
	b2_y = rand() % (220 - _SPACE_Y_) + 15;

	setup();
	while (ScreenFlip() == 0 && ProcessMessage() == 0 && ClearDrawScreen() == 0 &&
		updatekey() == 0 && GetMousePoint(&mouse_x, &mouse_y) == 0){
		
		input();
		scroll();
		jump();
		fall();
		if (check_hit() == 1){
			gameover = TRUE;
			break;
		}
		c_y = c_y_a;
		Draw();
	}
	//終了処理
	while (ScreenFlip() == 0 && ProcessMessage() == 0 && ClearDrawScreen() == 0 &&
		updatekey() == 0 && GetMousePoint(&mouse_x, &mouse_y) == 0){
		if (gameover == TRUE){
			gameover_back = GetColor(gameover_back_c,0,0);
			if (gameover_back_c > 0){
				gameover_back_c-= 15;
			}
			else{
				if (click() == 1){
					setup();
					break;
				}
			}
			DrawBox(0, 0, _WIDTH_, _DEPTH_,gameover_back, TRUE);
			DrawFormatString(_WIDTH_ / 2 - GetDrawFormatStringWidth("GaMe oVEr") / 2, _DEPTH_ / 2, red, "gAME oVER");
			DrawFormatString(_WIDTH_ / 2 - GetDrawFormatStringWidth("pLEASE cLICK tO rETRY") / 2, _DEPTH_ - 50, white, "pLEASE cLICK tO rETRY");
			DrawFormatString(_WIDTH_ / 2 - GetDrawFormatStringWidth("sCORE:%d", score) / 2, _DEPTH_ / 2 + 50, white, "sCORE:%d", score);
			
		}
	}
}

void jump(){
	int space = 10;
	if (f_jump == 1){
		c_y_a= (int)(start_y - (30 * sin(M_PI / 2 / space*jump_count)));
		jump_count++;

		if (jump_count == space){
			jump_count = 0;
			f_jump = 0;
		}
	}

}

void fall(){
	if (f_jump == 0){
		i_fall += 1;
		c_y_a += (int)(i_fall/2);
	}
}

void scroll(){
	g1_x -= _SPEED_;
	g2_x -= _SPEED_;
	if (g1_x < 0-_P_SIZE_X_){
		g1_x = g2_x + _P_SIZE_X_;
	}
	if (g2_x < 0 - _P_SIZE_X_){
		g2_x = g1_x + _P_SIZE_X_;
	}


	b1_x -= _SPEED_;
	b2_x -= _SPEED_;
	if (b1_x < 0 - _B_SIZE_X_){
		b1_x = b2_x + _SPACE_X_;
		b1_y = rand() % (220 - _SPACE_Y_) + 15;
		add_score = 0;
	}
	if (b2_x < 0 - _B_SIZE_X_){
		b2_x = b1_x + _SPACE_X_;
		b2_y = rand() % (220 - _SPACE_Y_) + 15;
		add_score = 0;
	}
	if (((b1_x + _B_SIZE_X_ < 30) && add_score == 0) || ((b2_x + _B_SIZE_X_ < 30) && add_score == 0)){
		score++;
		add_score = 1;
	}
}

int check_hit(void){
	if (_C_X_ + _C_SIZE_ > b1_x -10 && _C_X_ < b1_x + _B_SIZE_X_ + 10){
		if ((c_y_a < b1_y) || ( c_y_a + _C_SIZE_ > b1_y + _SPACE_Y_)){
			return 1;
		}
	}
	else if (_C_X_ + _C_SIZE_ > b2_x - 10&& _C_X_ < b2_x + _B_SIZE_X_ + 10){
		if ((c_y_a < b2_y) || (c_y_a + _C_SIZE_ > b2_y + _SPACE_Y_)){
			return 1;
		}

	}
	else if (c_y_a < 0 ||
		c_y_a + _C_SIZE_ > 250){
		return 1;

	}
	return 0;
}
